package com.hcl.com;

import java.util.ArrayList;
import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
	
			
	            ArrayList<Employee> employees = new ArrayList<>();
	        	Employee employee1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
	            employees.add(employee1);

	            Employee employee2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
	            employees.add(employee2);

	            Employee employee3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");
	            employees.add(employee3);

	            Employee employee4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
	            employees.add(employee4);

	            Employee employee5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
	            employees.add(employee5);
	            
	          while(true) {
	            	
	            	Scanner scanner = new Scanner(System.in);	
	            	System.out.println("Employee Details:"+"Choose your option from below: "+"\n"+"1. Display Details of employees."
	            	+"\n"+"2.Names of all employees in the sorted order."+"\n"
	            			+"3.Count of Employees from each city."+"\n"
	            	+"4. Monthly salary of each Employee."+"\n"+"5.End the process.");
	            	System.out.print("Choose your option from above ");
	    	        int chooseOption = scanner.nextInt();
	    	        
	    	        switch(chooseOption) {
	    	        	case 1: 
	    	        		//for list of empolyees
	    	            	System.out.println("\n"+"List of Employees:"+"\n\n"+"---------------------------------"+"\n\n"+"S.No"+"\t"+" NAME"+"\t"+"  AGE"+"\t"+"SALARY(INR)"+"\t"+"DEPARTMENT"+"\t"+"LOCATION");
	    	            	System.out.println("---------------------------------------------------------------");
	    	            	for(int i=0;i<5;i++) { 
	    	            		System.out.println(employees.get(i));
	    	            	}
	    	            	System.out.println("-----------------------------------------------------------------");
	    	            	System.out.println();
	    	                break;
	    	            
	    	        case 2:
	    	        		//Names of all employees in the sorted order are:
	    	            	DataStructureA dataStructureA = new DataStructureA();
	    	                
	    	            	System.out.println();
	    	            	dataStructureA.sortingNames(employees);
	    	            	System.out.println();
	    	           	break;
	    	            	
	    	          case 3:
	    	        		
	    	        		 DataStructureB  dataStructureB= new DataStructureB();
	    	                 
	    	        		 System.out.println();
	    	        		 dataStructureB.cityNameCount(employees);
	    	                 System.out.println();
	    	                 break;
	    	                 
	    	        	case 4:
	    	        		
	    	        		DataStructureB dataStructureB1 = new DataStructureB();
	    	        		
	    	        		System.out.println();
	    	        		dataStructureB1.monthlySalary(employees);
	    	                System.out.println();
	    	               break;
	    	              
	    	        	case 5:
	    	        		 System.exit(0);
	    	        
	    	         default:
	    	 	        	System.err.println("Please Enter the valid option."+"\n");
	    	            	
	   	
	    	        }
	            	
	            }
		}

	}


