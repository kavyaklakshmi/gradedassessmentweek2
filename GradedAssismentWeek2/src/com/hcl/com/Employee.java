package com.hcl.com;

public class Employee {
	int id;
	String name;
    int age;
    int salary; 
    String department;
    String city;

    public Employee(int id, String name, int age, int salary, String department, String city)throws IllegalArgumentExceptionHandle {
    	super();
    	
    	if(id<0)
    	{
    		throw new IllegalArgumentExceptionHandle("Please provide valid id ");
    	}
        this.id = id;

        if(name==null||name=="") {
        	throw new IllegalArgumentExceptionHandle("Please provide valid name otherthan null");
        }
        this.name = name;

        if(age<=0)
        {
        	throw new IllegalArgumentExceptionHandle("Please provide valid age");
        }
        this.age = age;

        if(salary<0)
        {
        	throw new IllegalArgumentExceptionHandle(" salary cannot be zero");
        }
        this.salary = salary;

        if( department == null||department =="") 
        {
        	throw new IllegalArgumentExceptionHandle("provide valid department");
        }
        this.department = department;

        if(city==null||city=="")
        		 
        {
            throw new IllegalArgumentExceptionHandle("provide valid city");
        }
        this.city = city;
    
    }
    

    public int getId() {
    	return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    
 @Override
 	public String toString() {
        return "  "+id +"\t"+name+"\t  "+age+"\t"+salary+"\t\t  "+department+"\t\t"+city;
 }

}
